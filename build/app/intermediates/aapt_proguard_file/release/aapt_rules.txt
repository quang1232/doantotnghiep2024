-keep class android.app.Application { <init>(); }
-keep class androidx.core.app.CoreComponentFactory { <init>(); }
-keep class com.datn.app_speech_to_text.MainActivity { <init>(); }
-keep class io.flutter.plugins.urllauncher.WebViewActivity { <init>(); }
-keep class androidx.browser.browseractions.BrowserActionsFallbackMenuView { <init>(android.content.Context, android.util.AttributeSet); }

