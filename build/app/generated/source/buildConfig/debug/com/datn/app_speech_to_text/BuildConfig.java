/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.datn.app_speech_to_text;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.datn.app_speech_to_text";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.0";
}
