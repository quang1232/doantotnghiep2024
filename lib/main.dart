import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';
import 'contact.dart';
import 'contact_controller.dart';


void main() => runApp(GetMaterialApp(
      home: MainScreen(),
    ));


class MainScreen extends StatelessWidget {
  const MainScreen({super.key});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: const Text("Speech to text"),
      ),
      body: const SpeechSampleApp(),
    );
  }

}

class SpeechSampleApp extends StatefulWidget {
  final Widget? body;
  const SpeechSampleApp({super.key, this.body});

  @override
  State<StatefulWidget> createState() {
    return SpeechSampleAppState(body);
  }
}

/// An example that demonstrates the basic functionality of the
/// SpeechToText plugin for using the speech recognition capability
/// of the underlying platform.
class SpeechSampleAppState extends State<SpeechSampleApp> {
  SpeechSampleAppState(this.body);
  final Widget? body;
  bool _hasSpeech = false;
  bool _logEvents = false;
  bool _onDevice = false;
  final TextEditingController _pauseForController = TextEditingController(text: '3');
  final TextEditingController _listenForController = TextEditingController(text: '10');
  double level = 0.0;
  double minSoundLevel = 50000;
  double maxSoundLevel = -50000;
  String lastWords = '';
  String lastError = '';
  String lastStatus = '';
  String _currentLocaleId = '';
  List<LocaleName> _localeNames = [];
  final SpeechToText speech = SpeechToText();

  @override
  void initState() {
    super.initState();
    _hasSpeech ? null : initSpeechState();
  }

  /// This initializes SpeechToText. That only has to be done
  /// once per application, though calling it again is harmless
  /// it also does nothing. The UX of the sample app ensures that
  /// it can only be called once.
  Future<void> initSpeechState() async {
    _logEvent('Initialize');
    try {
      var hasSpeech = await speech.initialize(
        onError: errorListener,
        onStatus: statusListener,
        debugLogging: _logEvents,
      );
      if (hasSpeech) {

        _localeNames = await speech.locales();

        var systemLocale = await speech.systemLocale();
        _currentLocaleId = systemLocale?.localeId ?? '';
      }
      if (!mounted) return;

      setState(() {
        _hasSpeech = hasSpeech;
      });
    } catch (e) {
      setState(() {
        lastError = 'Speech recognition failed: ${e.toString()}';
        _hasSpeech = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Column(
        children: <Widget>[
          const SizedBox(
            height: 8,
          ),
          SpeechControlWidget(
              _hasSpeech, speech.isListening, startListening, stopListening, cancelListening),
          SessionOptionsWidget(
            _currentLocaleId,
            _switchLang,
            _localeNames,
            _logEvents,
            _switchLogging,
            _pauseForController,
            _listenForController,
            _onDevice,
            _switchOnDevice,
          ),
        ],
      ),
      Expanded(
        child: RecognitionResultsWidget(
          lastWords: lastWords,
          level: level,
          body: body,
        ),
      ),
      Row(
        children: [
          Expanded(
            child: SpeechStatusWidget(speech: speech),
          ),
          // ErrorWidget(lastError: lastError),
        ],
      )
    ]);
  }

  // This is called each time the users wants to start a new speech
  // recognition session
  void startListening() {
    _logEvent('start listening');
    lastWords = '';
    lastError = '';
    final pauseFor = int.tryParse(_pauseForController.text);
    final listenFor = int.tryParse(_listenForController.text);
    // Note that `listenFor` is the maximum, not the minimum, on some
    // systems recognition will be stopped before this value is reached.
    // Similarly `pauseFor` is a maximum not a minimum and may be ignored
    // on some devices.
    speech.listen(
      onResult: resultListener,
      listenFor: Duration(seconds: listenFor ?? 10),
      pauseFor: Duration(seconds: pauseFor ?? 3),
      partialResults: true,
      localeId: _currentLocaleId,
      onSoundLevelChange: soundLevelListener,
      cancelOnError: true,
      listenMode: ListenMode.deviceDefault,
      onDevice: false,
    );
    setState(() {});
  }

  void stopListening() {
    _logEvent('stop');
    speech.stop();
    setState(() {
      level = 0.0;
    });
  }

  void cancelListening() {
    _logEvent('cancel');
    speech.cancel();
    setState(() {
      level = 0.0;
    });
  }

  /// This callback is invoked each time new recognition results are
  /// available after `listen` is called.
  void resultListener(SpeechRecognitionResult result) async{
    _logEvent('Result listener final: ${result.finalResult}, words: ${result.recognizedWords}');
    setState(()  {
      lastWords = '${result.recognizedWords} - ${result.finalResult}';
      if(result.finalResult){
          level = 0.0;
      }


      if(Get.isRegistered<ContactController>()){
        if(result.finalResult == true){ //nếu kết quả trả về là true thì sẽ gọi các hàm trong file contactcontroller
          Get.find<ContactController>().checkCallUser(result.recognizedWords.toLowerCase()); //gọi
          Get.find<ContactController>().checkMessageUser(result.recognizedWords.toLowerCase()); //nhantin
          Get.find<ContactController>().checkMessageWithContent(result.recognizedWords.toLowerCase()); //nhan tin voi noi dung
          stopListening();
          return;
        }
      }else{ //
        if (result.recognizedWords.toLowerCase() == "mở danh bạ" && result.finalResult == true) {
          Get.to(ListContact());
          stopListening();
        }
      }
    });
  }


  void soundLevelListener(double level) {
    minSoundLevel = min(minSoundLevel, level);
    maxSoundLevel = max(maxSoundLevel, level);
    // _logEvent('sound level $level: $minSoundLevel - $maxSoundLevel ');
    setState(() {
      this.level = level;
    });
  }

  void errorListener(SpeechRecognitionError error) {
    _logEvent('Received error status: $error, listening: ${speech.isListening}');
    setState(() {
      lastError = '${error.errorMsg} - ${error.permanent}';
    });
  }

  void statusListener(String status) {  //trạng thái nghe
    _logEvent('Received listener status: $status, listening: ${speech.isListening}');
    setState(() {
      lastStatus = status;
    });
  }

  void _switchLang(selectedVal) {  // khi switchlang bị gọi thi hàm currentLocaledId sẽ được cập nhật
    setState(() {
      _currentLocaleId = selectedVal;
    });
    debugPrint(selectedVal);
  }

  void _logEvent(String eventDescription) {
    if (_logEvents) {
      var eventTime = DateTime.now().toIso8601String();
      debugPrint('$eventTime $eventDescription');
    }
  }

  void _switchLogging(bool? val) {
    setState(() {
      _logEvents = val ?? false;
    });
  }

  void _switchOnDevice(bool? val) {  //khi switchOndevice được gọi thì giá trị ondeice sẽ được cập nhật
    setState(() {
      _onDevice = val ?? false;
    });
  }
}

/// Displays the most recently recognized words and the sound level.
class RecognitionResultsWidget extends StatelessWidget {
  const RecognitionResultsWidget({Key? key, required this.lastWords, required this.level, this.body}) : super(key: key);
  final Widget? body;
  final String lastWords;
  final double level;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                width: double.infinity,
                color: Theme.of(context).secondaryHeaderColor,
                child: Column(
                  children: [
                    Text(
                      lastWords,  // hiển thị nội dung của biến lastWords
                      textAlign: TextAlign.center,
                    ),
                    Expanded(child: body ?? const SizedBox(),)
                  ],
                ),
              ),
              Positioned.fill( // widget chứa icon mic
                bottom: 40,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: 40,
                    height: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(blurRadius: .26, spreadRadius: level * 1.5, color: Colors.black.withOpacity(.05))
                      ],
                      color: Colors.white,
                      borderRadius: const BorderRadius.all(Radius.circular(50)),
                    ),
                    child: IconButton(
                      icon: const Icon(Icons.mic),
                      onPressed: () {},
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({ //  hiển thị thông tin nhận diện giọng nói
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center( // hiển thị trong trung tâm của màn hình
      child: Text(
        'Speech recognition available',
        style: TextStyle(fontSize: 22.0),
      ),
    );
  }
}

/// Display the current error status from the speech
/// recognizer
class ErrorWidget extends StatelessWidget {
  const ErrorWidget({
    Key? key,
    required this.lastError,
  }) : super(key: key);

  final String lastError;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const Center(
          child: Text(
            'Error Status',
            style: TextStyle(fontSize: 22.0),
          ),
        ),
        Center(
          child: Text(lastError),
        ),
      ],
    );
  }
}

/// Controls to start and stop speech recognition
class SpeechControlWidget extends StatelessWidget {
  const SpeechControlWidget(
      this.hasSpeech, this.isListening, this.startListening, this.stopListening, this.cancelListening,
      {Key? key})
      : super(key: key);

  final bool hasSpeech;
  final bool isListening;
  final void Function() startListening;
  final void Function() stopListening;
  final void Function() cancelListening;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          const SizedBox(
            width: 8,
          ),
          Expanded(child: SizedBox(
            height: 50,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(backgroundColor: Colors.green),
              onPressed: !hasSpeech || isListening ? null : startListening,
              child: const Text('Start'),
            ),
          )),
          const SizedBox(
            width: 16,
          ),
          Expanded(child: SizedBox(
            height: 50,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
              onPressed: isListening ? stopListening : null,
              child: const Text('Stop'),
            ),
          ),),
          const SizedBox(
            width: 16,
          ),
          Expanded(child: SizedBox(
            height: 50,
            child: ElevatedButton(
              onPressed: isListening ? cancelListening : null,
              child: const Text('Cancel'),
            ),
          )),
          const SizedBox(
            width: 8,
          ),
        ],
      ),
    );
  }
}


class SessionOptionsWidget extends StatelessWidget {
  const SessionOptionsWidget(this.currentLocaleId, this.switchLang, this.localeNames, this.logEvents,
      this.switchLogging, this.pauseForController, this.listenForController, this.onDevice, this.switchOnDevice,
      {Key? key})
      : super(key: key);

  final String currentLocaleId;
  final void Function(String?) switchLang;
  final void Function(bool?) switchLogging;
  final void Function(bool?) switchOnDevice;
  final TextEditingController pauseForController;
  final TextEditingController listenForController;
  final List<LocaleName> localeNames;
  final bool logEvents;
  final bool onDevice;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: [
              const Text('Ngôn ngữ: '),
              DropdownButton<String>(
                onChanged: (selectedVal) => switchLang(selectedVal),
                value: currentLocaleId,
                items: localeNames
                    .map(
                      (localeName) => DropdownMenuItem(
                        value: localeName.localeId,
                        child: Text(localeName.name),
                      ),
                    )
                    .toList(),
              ),
            ],
          ),
          Row(
            children: [
              const Text('pauseFor: '),
              Container(
                  padding: const EdgeInsets.only(left: 8),
                  width: 80,
                  child: TextFormField(
                    controller: pauseForController,
                  )),
              Container(padding: const EdgeInsets.only(left: 16), child: const Text('listenFor: ')),
              Container(
                  padding: const EdgeInsets.only(left: 8),
                  width: 80,
                  child: TextFormField(
                    controller: listenForController,
                  )),
            ],
          ),
          // Row(
          //   children: [
          //     const Text('On device: '),
          //     Checkbox(
          //       value: onDevice,
          //       onChanged: switchOnDevice,
          //     ),
          //     const Text('Log events: '),
          //     Checkbox(
          //       value: logEvents,
          //       onChanged: switchLogging,
          //     ),
          //   ],
          // ),
        ],
      ),
    );
  }
}


/// Display the current status of the listener
class SpeechStatusWidget extends StatelessWidget {
  const SpeechStatusWidget({
    Key? key,
    required this.speech,
  }) : super(key: key);

  final SpeechToText speech;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20),
      color: Theme.of(context).colorScheme.background,
      child: Center(
        child: speech.isListening
            ? const Text(
                "Đang lắng nghe...",
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            : const Text(
                'Không nghe',  
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
      ),
    );
  }
}
