import 'dart:io';
import 'package:app_speech_to_text/toast.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:speech_to_text/speech_to_text.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactController extends GetxController {
  RxList<Contact> contacts = <Contact>[].obs;
  var isReady = false.obs;
  final SpeechToText speech = SpeechToText();
  String message = '';
  @override
  void onInit() async {
    fetchContacts(); // lấy danh bạ
    super.onInit();
  }

  Future<List<Contact>> getContacts() async { // kiểm tra quyền truy cập trên thiết bị
    List<Contact> contacts = [];
    if (Platform.isAndroid) {
      PermissionStatus permissionStatus = await Permission.contacts.status;
      if (permissionStatus.isGranted) {
        // Lấy danh sách danh bạ
        Iterable<Contact> contactsIterable = await ContactsService.getContacts();
        contacts = contactsIterable.toList();
      } else {
        // Yêu cầu quyền truy cập danh bạ nếu chưa có
        PermissionStatus permissionStatus = await Permission.contacts.request();
        if (permissionStatus.isGranted) {
          Iterable<Contact> contactsIterable = await ContactsService.getContacts();
          contacts = contactsIterable.toList();
        }
      }
    } else {
      Iterable<Contact> contactsIterable = await ContactsService.getContacts();
      contacts = contactsIterable.toList();
    }

    return contacts;
  }

  fetchContacts() async {
    isReady.value = false;
    contacts.value = await getContacts();

    // Sử dụng danh sách danh bạ ở đây
    for (var contact in contacts) {
      print('Tên: ${contact.displayName}');
      print('Số điện thoại: ${contact.phones}');
      print('-------------------');
    }
    isReady.value = true;
  }

  checkMessageWithContent(String text) async {
    bool isNoContact = false;
    try {
      int startIndex = text.indexOf("nhắn tin với") + "nhắn tin với".length;
      int endIndex = text.indexOf("với nội dung");
      if (startIndex >= 0 && endIndex >= 0 && startIndex <= endIndex) {
        String name = text.substring(startIndex, endIndex).trim();
        for (int i = 0; i < contacts.length; i++) {
          if (contacts[i].displayName!.toLowerCase() == name.toLowerCase()) {
            if (contacts[i].phones!.isNotEmpty) {
              try {
                int start = text.indexOf("với nội dung") + "với nội dung".length;
                String content = text.substring(start, text.length).trim();
                Uri sms = Uri(); // khai báo biến sms là một loạt url
                if (Platform.isAndroid) {
                  sms = Uri.parse('sms:+${contacts[i].phones![0].value}?body=$content');
                } else if (Platform.isIOS) {
                  sms = Uri.parse('sms:${contacts[i].phones![0].value}&body=$content');
                }
                if (await canLaunchUrl(sms)) {
                  launchUrl(sms);
                } else {
                  throw 'Could not launch';
                }
                isNoContact = true; // 
              } catch (e) {
                print(e);
              }
              return;
            } else {
              ToastUtil.show("người dùng không có số điện thoại liên lạc");
            }
            return;
          }
        }
        if (isNoContact) {
          ToastUtil.show("Không tìm thấy liên lac");
        }
      }
    } catch (e) {
      print(e);
    }
  }



  checkMessageUser(String text) {
    try{
      if (text.substring(0, 8) == "nhắn tin" && text.contains("với nội dung") == false) {
        messageDirectory(text.substring(9, text.length).trim());
      }
    }catch(e){
      print(e);
    }
  }

  messageDirectory(name) {
    for (int i = 0; i < contacts.length; i++) {
      if (contacts[i].displayName!.toLowerCase() == name.toLowerCase()) {
        if (contacts[i].phones!.isNotEmpty) {
          launchUrl(Uri(
            scheme: 'sms',
            path: contacts[i].phones![0].value,
          ));
          return;
        } else {
          ToastUtil.show("người dùng không có số điện thoại liên lạc");
        }
        return;
      }
    }
    ToastUtil.show("Không tìm thấy liên lac");
  }
  checkCallUser(String text) {
    try{
      if (text.substring(0, 3) == "gọi") {
        callDirectory(text.substring(4, text.length).trim());
      }
    }catch(e){
      print(e);
    }
  }

  callDirectory(String name) {
    for (int i = 0; i < contacts.length; i++) {
      if (contacts[i].displayName!.toLowerCase() == name.toLowerCase()) {
        if (contacts[i].phones!.isNotEmpty) {
          launchUrl(Uri(
            scheme: 'tel',
            path: contacts[i].phones![0].value,
          ));
          return;
        } else {
          ToastUtil.show("người dùng không có số điện thoại liên lạc");
        }
        return;
      }
    }
    ToastUtil.show("Không tìm thấy liên lac");
  }
}
